# Klaim Auth Service

A Auth service using NestJS 7.x and Passport Auth JWT System :heart_eyes_cat:

## Installation

```bash
   $ npm install
```

## Set Enviroment for secret key JWT

```bash
   $ cp .env.example .env
```

## Config settings .env for send notification when a user registers, forgot password or change password

```
   EMAIL_HOST=smtp.mailtrap.io
   EMAIL_PORT=2525
   EMAIL_AUTH_USER=[:user]
   EMAIL_AUTH_PASSWORD=[:password]
   SECRET_KEY_JWT="secretOrKey"
   POSTGRES_HOST=127.0.0.1
   POSTGRES_PORT=5433
   POSTGRES_USER=klaim
   POSTGRES_PASSWORD=klaim
   POSTGRES_DATABASE=klaim
   PORT=4200
   MODE=DEV
   RUN_MIGRATIONS=true
```
## Install TypeScript Node

```bash
   $ npm install -g ts-node
```

## Running migrations with typeorm

```bash
   $ ts-node node_modules/.bin/typeorm migration:run
```

or

```bash
   $ node_modules/.bin/typeorm migration:run
```

## Running the app

```bash
    # development
    $ npm run start

    # watch mode
    $ npm run start:dev

    # production mode
    $ npm run start:prod
```

## Docker

There is a `docker-compose.yml` file for starting MySQL with Docker.

`$ docker-compose up`

After running, you can stop the Docker container with

`$ docker-compose down`
