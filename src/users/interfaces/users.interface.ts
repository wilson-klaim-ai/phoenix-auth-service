export interface IUsers {
  readonly id: number;
  readonly username: string;
  readonly email: string;
  readonly password: string;
  readonly token: string;
  readonly createdAt: Date;
  readonly updateAt: Date;
}
