FROM node:latest

COPY . /home/auth-service

WORKDIR /home/auth-service

RUN npm install

EXPOSE 4200

CMD npm -v